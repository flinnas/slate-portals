<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:fw="http://technolutions.com/framework" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xhtml">
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <template path="/shared/base.xslt" xmlns="http://technolutions.com/framework" />

        <head>
            <link href="/shared/build-fonts.css?v=20181016113059" rel="stylesheet" />
            <link href="/shared/build-mobile-global.css" rel="stylesheet" />
            <script src="/shared/build-mobile-global.js" />
            <link href="/shared/build.css" rel="stylesheet" />
        <style>html &gt; body { line-height: normal; } ul.cr, li.cr { margin: 0; padding: 0; } #content { clear: both; padding: 15px; } #global { float: right; } #global ul, #global li { list-style: none; margin: 0; padding: 0; }</style>
        <meta name="viewport" content="width=device-width" class="cr" />
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" media="all" class="cr" />
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:400,400i,600,700" media="all" class="cr" />
        <xsl:apply-templates select="xhtml:html/xhtml:head/node()" />
      </head>
      <body>
        <xsl:copy-of select="xhtml:html/xhtml:body/@*" />
        <div id="page">
          <div class="c_html c_not-front c_not-logged-in c_no-sidebars c_page-node c_page-node- c_page-node-5057 c_node-type-page c_section-clean-slate c_page-panels cr" style="">
            <p id="c_skip-link" class="cr">
              <a href="https://admissions.wwu.edu/clean-slate#main-menu" class="c_element-invisible c_element-focusable cr">Jump to navigation</a>
            </p>
            <div class="c_region c_region-page-top cr" />
            <div class="c_page cr">
              <div class="c_bluebar cr" />
              <header role="banner" class="cr">
                <section class="c_western-header cr" aria-label="University Links, Search, and Navigation">
                  <div class="c_center-content cr">
                    <span class="c_western-logo cr">
                      <a href="http://www.wwu.edu/" class="cr">Western Washington University</a>
                    </span>
                    <span class="c_site-name cr">
                      <a href="https://admissions.wwu.edu/" class="cr">Admissions</a>
                    </span>
                    <!-- <nav role="navigation" aria-label="WWU menu" class="cr">
                      <div class="c_western-quick-links cr" aria-label="Western Quick Links">
                        <button role="button" aria-pressed="false" aria-label="Toggle Quick Links" class="cr">Toggle Quick Links</button>
                        <ul class="cr">
                          <li class="cr">
                            <a href="http://www.wwu.edu/academic_calendar" title="Calendar" class="cr">
                              <span aria-hidden="true" class="cr">c</span>
                              <span class="cr">Calendar</span>
                            </a>
                          </li>
                          <li class="cr">
                            <a href="http://www.wwu.edu/directory" title="Directory" class="cr">
                              <span aria-hidden="true" class="cr">d</span>
                              <span class="cr">Directory</span>
                            </a>
                          </li>
                          <li class="cr">
                            <a href="http://www.wwu.edu/index" title="Index" class="cr">
                              <span aria-hidden="true" class="cr">i</span>
                              <span class="cr">Index</span>
                            </a>
                          </li>
                          <li class="cr">
                            <a href="http://www.wwu.edu/campusmaps" title="Map" class="cr">
                              <span aria-hidden="true" class="cr">l</span>
                              <span class="cr">Map</span>
                            </a>
                          </li>
                          <li class="cr">
                            <a href="http://mywestern.wwu.edu/" title="myWestern" class="cr">
                              <span aria-hidden="true" class="cr">w</span>
                              <span class="cr">myWestern</span>
                            </a>
                          </li>
                        </ul>
                      </div>
                     <button class="c_mobile-main-nav cr" role="button" aria-pressed="false" aria-label="Open Mobile Main Navigation">Open Main Navigation</button>
                    </nav>-->
                  </div>
                </section>
                <div class="c_site-header-wrapper cr">
                  <section class="c_site-header cr" aria-label="Site Header">
                    <a href="https://admissions.wwu.edu/" title="Home" rel="home" class="cr" />
                    <div class="c_site-banner cr">
                      <img src="/images/portal/admissions-header-fall.jpg" alt="" class="cr" />
                    </div>
                    <div class="c_site-name cr">
                      <p class="cr">
                        <span class="cr">Admissions</span>
                      </p>
                    </div>
                  </section>
                  <nav class="c_main-nav cr" id="c_main-menu" role="navigation" aria-label="Main admissions navigation">
                    <div class="c_region c_region-navigation cr">
                      <div id="c_block-menu-block-14" class="c_block c_block-menu-block c_first c_last c_odd cr" role="navigation">
                        <div class="c_menu-block-wrapper c_menu-block-14 c_menu-name-main-menu c_parent-mlid-0 c_menu-level-1 cr">
                          <ul class="c_menu cr">
                            <li class="c_menu__item c_is-leaf c_first c_leaf c_menu-mlid-1187 cr">
                              <a href="https://admissions.wwu.edu/" class="c_menu__link cr">Home</a>
                            </li>
                            <li class="c_menu__item c_is-expanded c_expanded c_menu-mlid-1137 cr">
                              <a href="https://admissions.wwu.edu/apply" class="c_nolink cr">Apply</a>
                            </li>
                            <li class="c_menu__item c_is-expanded c_expanded c_menu-mlid-1141 cr">
                              <a href="https://admissions.wwu.edu/academics" class="c_menu__link cr">Academics</a>
                            </li>
                            <li class="c_menu__item c_is-expanded c_expanded c_menu-mlid-2867 cr">
                              <a href="https://admissions.wwu.edu/bellingham" class="c_nolink cr">Student Life</a>
                            </li>
                            <li class="c_menu__item c_is-expanded c_expanded c_menu-mlid-1146 cr">
                              <a href="https://admissions.wwu.edu/tuition-expenses" class="c_nolink cr">Tuition &amp; Scholarships</a>
                            </li>
                            <li class="c_menu__item c_is-expanded c_expanded c_menu-mlid-1149 cr">
                              <a href="https://admissions.wwu.edu/visit" class="c_nolink cr">Visit</a>
                            </li>
                            <li class="c_menu__item c_is-expanded c_expanded c_menu-mlid-1175 cr">
                              <a href="https://admissions.wwu.edu/admitted-student-checklist" class="c_nolink cr">Admitted Students</a>
                            </li>
                            <li class="c_menu__item c_is-expanded c_last c_expanded c_menu-mlid-1184 cr">
                              <a href="https://admissions.wwu.edu/contact-info" class="c_nolink cr">Contact Us</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </nav>
                </div>
              </header>
              <main role="main" class="cr">
                <section class="c_content c_column cr">
                  <div class="c_center-content cr">
                    <div id="global" />
                    <div id="content">
                      <xsl:apply-templates select="xhtml:html/xhtml:body/node()" />
                    </div>
                    <div class="c_wwu-75-percent-right-column cr">
                      <div class="cr">
                        <div class="c_panel-pane c_pane-node-content cr">
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </main>
            </div>
            <footer role="contentinfo" class="cr">
              <div class="c_footer-wrapper cr">
                <div class="c_footer-left cr">
                  <div class="c_region c_region-footer-left cr">
                    <div id="c_block-block-2" class="c_block c_block-block c_first c_last c_odd cr">
                      <p style="text-align: center;" class="cr">
                        <a href="https://admissions.wwu.edu/" style="color:#fff;" class="cr">Office of Admissions</a>
                      </p>
                      <div style="text-align: center;" class="cr">
                        <img alt="Blue Ribbon" style="height:100px; width:100px" class="c_file-preview c_media-element cr" typeof="foaf:Image" src="/images/big_ribbon_web.png?itok=oD6wkpEH" width="100" height="100" title="" />
                      </div>
                      <p style="text-align: center;" class="cr">
                        <a href="http://www.wwu.edu/about/recognition" style="color:#fff;" class="cr">National Recognition for Western</a>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="c_footer-center cr">
                  <div class="c_region c_region-footer-center cr">
                    <div id="c_block-menu-menu-center-footer-menu" class="c_block c_block-menu c_first c_last c_odd cr" role="navigation">
                      <ul class="c_menu cr">
                        <li class="c_menu__item c_is-leaf c_first c_leaf cr">
                          <a href="https://admissions.wwu.edu/contact-us" class="c_menu__link cr">Contact Admissions</a>
                        </li>
                        <li class="c_menu__item c_is-leaf c_leaf cr">
                          <a href="https://www.applyweb.com/public/input?s=wwuinq" class="c_menu__link cr">Request Info</a>
                        </li>
                        <li class="c_menu__item c_is-leaf c_leaf cr">
                          <a href="http://www.wwu.edu/academic_calendar" class="c_menu__link cr">Academic Calendar</a>
                        </li>
                        <li class="c_menu__item c_is-leaf c_last c_leaf cr">
                          <a href="http://www.wwu.edu/eoo/" class="c_menu__link cr">AA/EO</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="c_western-privacy-statement cr">
                    <a href="http://www.wwu.edu/privacy/" class="cr">Website Privacy Statement</a>
                    <br class="cr" />
                    <a href="https://www.wwu.edu/commitment-accessibility" class="cr">Accessibility</a>
                  </div>
                </div>
                <div class="c_footer-right cr" role="complementary" aria-label="WWU Contact Info">
                  <h1 class="cr">
                    <a href="http://www.wwu.edu/" class="cr">Western Washington University</a>
                  </h1>
                  <div class="c_western-contact-info cr">
                    <p class="cr">
                      <span aria-hidden="true" class="c_western-address cr" />516 High Street<br class="cr" /><span class="c_western-address-city cr">Bellingham, WA 98225</span></p>
                    <p class="cr">
                      <span aria-hidden="true" class="c_western-telephone cr" />
                      <a href="tel:3606503000" class="cr">(360) 650-3000</a>
                    </p>
                    <p class="cr">
                      <span aria-hidden="true" class="c_western-contact cr" />
                      <a href="http://www.wwu.edu/wwucontact/" class="cr">Contact Western</a>
                    </p>
                  </div>
                  <div class="c_western-social-media cr">
                    <ul class="cr">
                      <li class="cr">
                        <a href="https://social.wwu.edu/" class="cr">
                          <span aria-hidden="true" class="c_westernIcons-WwuSocialIcon cr" />Western Social</a>
                      </li>
                      <li class="cr">
                        <a href="http://www.facebook.com/westernwashingtonuniversity" class="cr">
                          <span aria-hidden="true" class="c_westernIcons-FacebookIcon cr" />Facebook</a>
                      </li>
                      <li class="cr">
                        <a href="http://www.flickr.com/wwu" class="cr">
                          <span aria-hidden="true" class="c_westernIcons-FlickrIcon cr" />Flickr</a>
                      </li>
                      <li class="cr">
                        <a href="https://twitter.com/WWU" class="cr">
                          <span aria-hidden="true" class="c_westernIcons-TwitterIcon cr" />Twitter</a>
                      </li>
                      <li class="cr">
                        <a href="http://www.youtube.com/wwu" class="cr">
                          <span aria-hidden="true" class="c_westernIcons-YouTubeIcon cr" />Youtube</a>
                      </li>
                      <li class="cr">
                        <a href="http://news.wwu.edu/go/feed/1538/ru/atom/" class="cr">
                          <span aria-hidden="true" class="c_westernIcons-RSSicon cr" />RSS</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </footer>
            <div id="c_cboxOverlay" style="display: none;" class="cr" />
            <div id="c_colorbox" class="cr" role="dialog" tabindex="-1" style="display: none;">
              <div id="c_cboxWrapper" class="cr">
                <div class="cr">
                  <div id="c_cboxTopLeft" style="float: left;" class="cr" />
                  <div id="c_cboxTopCenter" style="float: left;" class="cr" />
                  <div id="c_cboxTopRight" style="float: left;" class="cr" />
                </div>
                <div style="clear: left;" class="cr">
                  <div id="c_cboxMiddleLeft" style="float: left;" class="cr" />
                  <div id="c_cboxContent" style="float: left;" class="cr">
                    <div id="c_cboxTitle" style="float: left;" class="cr" />
                    <div id="c_cboxCurrent" style="float: left;" class="cr" />
                    <button type="button" id="c_cboxPrevious" class="cr" />
                    <button type="button" id="c_cboxNext" class="cr" />
                    <button type="button" id="c_cboxSlideshow" class="cr" />
                    <div id="c_cboxLoadingOverlay" style="float: left;" class="cr" />
                    <div id="c_cboxLoadingGraphic" style="float: left;" class="cr" />
                  </div>
                  <div id="c_cboxMiddleRight" style="float: left;" class="cr" />
                </div>
                <div style="clear: left;" class="cr">
                  <div id="c_cboxBottomLeft" style="float: left;" class="cr" />
                  <div id="c_cboxBottomCenter" style="float: left;" class="cr" />
                  <div id="c_cboxBottomRight" style="float: left;" class="cr" />
                </div>
              </div>
              <div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;" class="cr" />
            </div>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" />
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
